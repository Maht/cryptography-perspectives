from utils import is_english_sentence, apply_masc, generate_random_key, swap_random_letters
from collections import Counter
import string

# Bigram frequencies in typical English text
bigram_freq = {
    'TH': 1.52, 'HE': 1.28, 'IN': 0.94, 'ER': 0.94, 'AN': 0.82, 'RE': 0.68, 'ND': 0.63, 'AT': 0.59,
    'ON': 0.57, 'NT': 0.56, 'HA': 0.56, 'ES': 0.56, 'ST': 0.55, 'EN': 0.55, 'ED': 0.53, 'TO': 0.52,
    'IT': 0.50, 'OU': 0.50, 'EA': 0.47, 'HI': 0.46, 'IS': 0.46, 'OR': 0.43, 'TI': 0.34, 'AS': 0.33,
    'TE': 0.27, 'ET': 0.19, 'NG': 0.18, 'OF': 0.16, 'AL': 0.09, 'DE': 0.09, 'SE': 0.08, 'LE': 0.08,
    'SA': 0.08, 'SI': 0.05, 'AR': 0.04, 'VE': 0.04, 'RA': 0.04, 'LD': 0.02, 'UR': 0.02
}

common_keys = [
    "ETAOINSHRDLCUMWFGYPBVKJXQZ",  # Most frequent letters in English
    "ZQXJKVBPYGFWMUCLDRHSNIOATE",  # Least frequent to most frequent
    "QWERTYUIOPASDFGHJKLZXCVBNM",  # Keyboard layout
    "MNBVCXZLKJHGFDSAPOIUYTREWQ"   # Reverse keyboard layout
]

class MASCEncrypter:
    def __init__(self, key=None):
        if key is None:
            key = generate_random_key()
        self.key = key

    def encrypt(self, plaintext):
        return apply_masc(plaintext, self.key)

    def decrypt(self, ciphertext):
        reverse_key = {v: k for k, v in zip(string.ascii_uppercase, self.key)}
        return apply_masc(ciphertext, ''.join(reverse_key[char] for char in string.ascii_uppercase))


class MASCDecrypter:
    def __init__(self, ciphertext, iterations=1000):
        self.ciphertext = ciphertext
        self.iterations = iterations

    def bigram_score(self, text):
        text = text.replace(" ", "")
        bigrams = [text[i:i+2] for i in range(len(text) - 1)]
        text_bigram_freq = Counter(bigrams)
        score = sum(bigram_freq.get(bigram, 0) * count for bigram,
                    count in text_bigram_freq.items())
        return score

    def try_common_keys(self):
        for key in common_keys:
            decrypted_text = apply_masc(self.ciphertext, key)
            # print(is_english_sentence(decrypted_text))
            print("Decrypted text using {0}: {1}".format(key, decrypted_text))
            if is_english_sentence(decrypted_text):
                return decrypted_text, key
        return None, None

    def decrypt(self):
        # First try common keys
        decrypted_text, best_key = self.try_common_keys()
        if is_english_sentence(decrypted_text):
            # print("Decryption using common key:")
            return decrypted_text, best_key

        # If common keys fail, proceed with hill climbing
        print("Trying to decrypt using hill climbing...")
        attempt_count = 0
        best_key = generate_random_key()
        best_score = self.bigram_score(apply_masc(self.ciphertext, best_key))
        while True:
            attempt_count += 1
            new_key = swap_random_letters(best_key)
            new_score = self.bigram_score(apply_masc(self.ciphertext, new_key))
            if new_score > best_score:
                best_key, best_score = new_key, new_score

            decrypted_text = apply_masc(self.ciphertext, best_key)
            print(f"Attempt: {attempt_count}, Best Score: {best_score}, Decrypted Text: {decrypted_text[:50]}...")
            if decrypted_text == is_english_sentence(decrypted_text):
                break

            if attempt_count % 1000 == 0:
                print(f"Attempts: {attempt_count}, Best Score: {best_score}, Decrypted Text: {decrypted_text[:50]}...")

            if attempt_count == self.iterations:
                break
        print(f"Total attempts: {attempt_count}")
        return decrypted_text, best_key
