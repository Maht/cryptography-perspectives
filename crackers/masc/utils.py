import random
import string

import spacy
from spacy.lang.en.stop_words import STOP_WORDS

# Load the English model
nlp = spacy.load("en_core_web_sm")


def is_english_sentence(text):
    try:
        # Process the text
        doc = nlp(text)
    except:
        return False

    # Check if the text is English by verifying if the model processed it correctly
    if not doc.is_parsed:
        return False

    # Check for the presence of at least one noun and one verb
    has_noun = any(token.pos_ == "NOUN" for token in doc)
    has_verb = any(token.pos_ == "VERB" for token in doc)

    if not (has_noun and has_verb):
        return False

    # Check for the presence of stop words (common English words)
    has_stop_words = any(token.is_stop for token in doc)

    if not has_stop_words:
        return False

    # Check for a minimum length (to avoid random short strings)
    if len(doc) < 5:
        return False

    # Check for proper sentence structure
    has_proper_structure = any(
        token.dep_ in {"nsubj", "ROOT", "dobj"} for token in doc)

    if not has_proper_structure:
        return False

    return True

def apply_masc(text, key):
    # print("Text: ", text)
    # print("Trying key: ", key, "...")
    decrypted_text:str = text.translate(str.maketrans(key, string.ascii_uppercase))
    # print("Decrypted text: ", decrypted_text)
    return decrypted_text

def generate_random_key():
    alphabet = list(string.ascii_uppercase)
    random.shuffle(alphabet)
    return ''.join(alphabet)

def swap_random_letters(key):
    key = list(key)
    i, j = random.sample(range(len(key)), 2)
    key[i], key[j] = key[j], key[i]
    return ''.join(key)
