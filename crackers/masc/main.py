from masc_encrypter import MASCEncrypter, MASCDecrypter

if __name__ == "__main__":
    ciphertext = "EGRTETFXOFRTFHITWTZOETFLXWLZOZXZOGFLDTRTZGTZTBZWNTFTFNMOFUFTZZTKXKTJXTFEOTL"
    target_decrypted_text = "CODE CAN FIND ALPHABETICAL SUBSTITUTIONS MADE TO A TEXT BY ANALYZING LETTER FREQUENCIES"
    # target_decrypted_text = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"

    # Encrypt using provided key for validation
    provided_key = "TWERTXUIOPAFDFGHJKLZXCVBNM"
    encrypter = MASCEncrypter(provided_key)
    encrypted_text = encrypter.encrypt(target_decrypted_text)
    print("Encryption using provided key:", encrypted_text)
    # ciphertext = encrypted_text
    # decrypted_text_with_provided_key = encrypter.decrypt(ciphertext)
    # print("Decryption using provided key:")
    # print(f"Provided key: {provided_key}")
    # print(f"Decrypted text: {decrypted_text_with_provided_key}")

    # Attempt to crack the cipher
    decrypter = MASCDecrypter(ciphertext, iterations=1000)
    decrypted_text, best_key = decrypter.decrypt()
    print(f"Decryption completed.")
    print(f"Best key: {best_key}")
    print(f"Decrypted text: {decrypted_text}")
