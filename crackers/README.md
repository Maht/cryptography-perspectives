# crackers

This repository contains a Python script that generates a scientific paper based on a Proof of Concept (PoC) for the Caesar cipher and Masc cipher.

## Usage

1 -> Clone this repository:

```bash
git clone https://gitlab.com/Maht/cryptography-perspectives.git
```

2 -> Navigate to the project directory:

```bash
cd crackers
```

3 -> Stage the python virtual environment (with venv) in the OS

```bash
python -m venv env
```

4 -> Activate the virtual environment

```bash
# Windows
env\Scripts\activate
# Linux
source env/bin/activate
# MacOS
source env/bin/activate
```

5 -> Install the dependencies

```bash
pip install -r requirements.txt
```

6 -> Run the script of your choice:

```bash
# Caesar cipher
python caesar/main.py
# Masc cipher
python masc/main.py
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.
