import string

def caesar_cipher_decrypt(ciphertext, shift):
    decrypted_text = ""
    for char in ciphertext:
        if char.isalpha():
            shift_base = ord('A') if char.isupper() else ord('a')
            decrypted_text += chr((ord(char) - shift_base -
                                  shift) % 26 + shift_base)
        else:
            decrypted_text += char
    return decrypted_text


def is_english_word(word):
    # A simple dictionary check. You can use a larger dictionary for better accuracy.
    common_words = ["the", "be", "to", "of", "and", "a", "in", "that", "have",
                    "i", "it", "for", "not", "on", "with", "he", "as", "you", "do",
                    "at", "over", "by", "this", "they", "we", "say", "her", "she",
                    "or", "an", "will", "my", "one", "all", "would", "there", "their",
                    "what", "so", "up", "out", "if", "about", "who", "get", "which",
                    "go", "me", "when", "make", "can", "like", "time", "no", "just",
                    "quick", "up", "go", "take", "come", "know", "see", "come", "think"
                    "fox", "are", "day", "did", "now", "how", "down", "find", "from",
                    "brown", "use", "may", "water", "first", "been", "call", "who"]
    print(word.lower() in common_words)
    return word.lower() in common_words


def is_english_sentence(sentence):
    words:list[str] = sentence.split()
    print(words)
    if len(words) == 0:
        return False
    count = sum(is_english_word(word.strip(string.punctuation))
                for word in words)
    print(count / len(words))
    return count / len(words) > 0.5


def break_caesar_cipher(ciphertext):
    for shift in range(1, 26):
        decrypted_text = caesar_cipher_decrypt(ciphertext, shift)
        if is_english_sentence(decrypted_text):
            print(f"Decrypted text: {decrypted_text}")
            print(f"Shift used: {shift}")
            return decrypted_text, shift

    return None, None


ciphertext = "Gur dhvpx oebja sbk whzcf bire gur ynml qbt."
decrypted_text, shift = break_caesar_cipher(ciphertext)
print(decrypted_text, shift)
if decrypted_text:
    print(f"Original text: {decrypted_text}")
    print(f"Shift used: {shift}")
else:
    print("Failed to decrypt the text.")
