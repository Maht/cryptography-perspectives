package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"time"
)

const chunkSize = 190 // Adjust this based on your key size and padding scheme

func main() {
	// Read the file to be encrypted
	fileContent, err := readFile("file_to_encrypt.txt")
	if err != nil {
		fmt.Println("Error reading file:", err)
		return
	}

	// Encrypt the file using ECDH
	ecdhStart := time.Now()
	ecdhCiphertext, err := encryptFileECDH(fileContent)
	if err != nil {
		fmt.Println("Error encrypting file with ECDH:", err)
		return
	}
	ecdhDuration := time.Since(ecdhStart)
	fmt.Println("ECDH encryption time:", ecdhDuration)
	fmt.Printf("ECDH ciphertext length: %d bytes\n", len(ecdhCiphertext))

	// Encrypt the file using RSA
	rsaStart := time.Now()
	rsaCiphertext, err := encryptFileRSA(fileContent)
	if err != nil {
		fmt.Println("Error encrypting file with RSA:", err)
		return
	}
	rsaDuration := time.Since(rsaStart)
	fmt.Println("RSA encryption time:", rsaDuration)
	fmt.Printf("RSA ciphertext length: %d bytes\n", len(rsaCiphertext))
}

func readFile(filename string) ([]byte, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		return nil, err
	}

	data := make([]byte, stat.Size())
	_, err = io.ReadFull(file, data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func encryptFileECDH(data []byte) ([]byte, error) {
	// Generate ECDSA keys
	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, err
	}

	// Generate ephemeral ECDSA key
	ephemeralPrivateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, err
	}

	// Generate shared secret
	sharedSecretX, _ := elliptic.P256().ScalarMult(privateKey.PublicKey.X, privateKey.PublicKey.Y, ephemeralPrivateKey.D.Bytes())
	sharedSecret := sha256.Sum256(sharedSecretX.Bytes())

	// Encrypt the data using AES
	block, err := aes.NewCipher(sharedSecret[:])
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = rand.Read(nonce); err != nil {
		return nil, err
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)

	return ciphertext, nil
}

func encryptFileRSA(data []byte) ([]byte, error) {
	// Generate RSA keys
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}
	publicKey := &privateKey.PublicKey

	// Encrypt the data in chunks
	var encryptedData []byte
	for start := 0; start < len(data); start += chunkSize {
		end := start + chunkSize
		if end > len(data) {
			end = len(data)
		}
		chunk := data[start:end]
		encryptedChunk, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, publicKey, chunk, nil)
		if err != nil {
			return nil, err
		}
		encryptedData = append(encryptedData, encryptedChunk...)
	}

	return encryptedData, nil
}
